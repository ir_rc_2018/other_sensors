#!/usr/bin/env python

'''
WiFi message format from 'iwlist scan'
----------------------------------------------------------
	Cell ## - Address: ##:##:##:##:##:##
			  Channel: ...
			  Frequency: ...
			  Quality: ...   Signal level=-## dBm
		 	  Encryption key: ...
		 	  ESSID: "..."
		 	  ....
----------------------------------------------------------
'''
import rospy

import sys
import subprocess
from time import sleep
from std_msgs.msg import Float32 , Float32MultiArray


import serial



'''
global variables
'''
pub_wifi_rssi = rospy.Publisher('/wifi_rssi', Float32, queue_size=5)
pub_sonic_distance = rospy.Publisher('/distance_from_ultra', Float32MultiArray, queue_size=5)

arduino_device_port = '/dev/ttyACM1'
baud_rate = 9600
ser = serial.Serial(arduino_device_port,baud_rate)

'''
find a keyword in lines of WiFi message
'''
def matching_line(lines, keyword):
	for line in lines:
		matching = match(line, keyword)
		if matching != None:
			return matching

	return None

'''
get ESSID from WiFi message
'''
def get_name(cell):
	return matching_line(cell, "ESSID:")[1:-1]

'''
get MAC address from WiFi message
'''
def get_address(cell):
	return matching_line(cell, "Address: ")

'''
get RSSI(Received Signal Strength Indication) value from WiFi message
'''
def get_dBm(cell):
	level = matching_line(cell, "Signal level=")
	level = int(level[0:3].rstrip())
	return level

# rules to use in function parse_cell()
rules = {"Name" : get_name, "Address" : get_address, "Signal level" : get_dBm}


'''
parse each cell(WiFi message) using rules and above functions
'''
def parse_cell(cell):
	parsed_cell = {}
	for key in rules:
		rule = rules[key]
		parsed_cell.update({key:rule(cell)})

	return parsed_cell

'''
find a keyword from a line
'''
def match(line, keyword):
	line = line.lstrip()
	length = len(keyword)

	if keyword == "Signal level=":
		line = line[13:]
		line = line.lstrip()

	if line[:length] == keyword:
		return line[length:]
	else:
		return None

'''
get output message of 'iwlist scan'
parse each cell and construct dictionary of each cell
find user WiFi signal among cells(=WiFis)
'''
def findWifi(result):
	cells = [[]]
	parsed_cells = []

	result = result.split('\n')

	for line in result:
		cell_line = match(line, "Cell ")
		if cell_line != None :
			cells.append([])
			line = cell_line[-27:]
		cells[-1].append(line.rstrip())

	cells = cells[1:]

	for cell in cells:
		parsed_cells.append(parse_cell(cell))

	# find user device using user device's MAC address
	for device in parsed_cells:
		if device['Address'] == 'D0:13:FD:24:E4:6A' : #'52:77:05:FD:6C:9C':	# NEED TO BE CHANGED!!!!
			# change direction using ROS message
			#print(device)	# test code. NEED TO BE CHANGED!!!!
			return device['Signal level']



	print('scan complete') # test code
	return None

def wifi_timer(event) :
	global pub_wifi_rssi

	proc = subprocess.Popen(['sudo','iwlist', 'scan'], stdout=subprocess.PIPE)
	out, err = proc.communicate()

	rssi = findWifi(out)
	if rssi != None :
		pub_wifi_rssi.publish(rssi)
	else :
		pub_wifi_rssi.publish(10000)


def ultrasonic_timer(event) :
	global pub_sonic_distance, ser
	
	str_distance_arr = ser.readline()

	#print 	str_distance_arr

	msg = Float32MultiArray()

	str_distances = str_distance_arr.split(',')
	if len(str_distances) == 1 or  len(str_distances) == 3 :
		for idx in range(len(str_distances)) :
			distance_to_object = 10000.				
			try :
				distance_to_object = float(str_distances[idx])
			except ValueError ,e:
				print 'error',e,'str_distance',str_distances[idx]
			msg.data.append(distance_to_object)
			
	#try :
	#	 distance_to_object = float(str_distance)
	#except ValueError ,e:
	#	print 'error',e,'str_distance',str_distance



	pub_sonic_distance.publish(msg)


'''
main function
run 'iwlist scan' and pass output messages to findWifi function
'''
if __name__ == '__main__':
	try:
		rospy.init_node('wifi_untrasonic_sensors', anonymous=True)

		_wifi_timer = rospy.Timer(rospy.Duration(1), wifi_timer)
		_untrasonic_timer = rospy.Timer(rospy.Duration(0.5), ultrasonic_timer)


		rospy.spin()
		_wifi_timer.shutdown()
		_untrasonic_timer.shutdown()
	except rospy.ROSInterruptException:
		pass
